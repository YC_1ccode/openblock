/**
 * @license
 * Copyright 2021 Du Tian Wei
 * SPDX-License-Identifier: Apache-2.0
 */
import { agent } from './agent.mjs';
export default class installhap {
    static async installHap(hap) {
        await agent.scan();
        await Promise.any([agent.waitForOnline(), r => setTimeout(r, 10000)]);
        if (agent.getState() !== 'online') {
            alert("未检测到代理，或者代理版本过低，无法安装HAP。");
            open('../static/obburner_0.3.zip');
            return;
        }
        await agent.waitForOnline();
        if (agent.getVersion() < 0.3) {
            alert("代理版本过低，无法安装HAP。");
            open('../static/obburner_0.3.zip');
            return;
        }
        await agent.cmd(hap, ['hdc_bm_install.bat', "hnext.roar.com"]);
    }
}