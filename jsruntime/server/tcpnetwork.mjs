
OpenBlock.Env.registerEvents([''], 'tcpnetwork.h', [
    { "name": "NetworkError", "argType": "String" },
    { "name": "ServiceError", "argType": "String" },
]);
OpenBlock.Env.registerNativeFunction(['server'], 'TCPNetwork-Server', 'TCPNetwork-Server', [
    {
        method_name: 'listenClientMessage', arguments: [], returnType: 'void'
    },
    {
        method_name: 'isNetworkMessage', arguments: [], returnType: 'Boolean'
    },
]);
OpenBlock.Env.registerNativeTypes([''], { 'TCP_Server': [] });
OpenBlock.Env.registerEvents([''], 'TCPNetwork-Client', [
    { "name": "onTcpServerConnected", "argType": "TCP_Server" },
]);
OpenBlock.Env.registerNativeFunction([''], 'TCPNetwork-Client', 'TCPNetwork-Client', [
    {
        method_name: 'connectToServer', arguments: [
            { type: 'String', name: 'server_sence', field: { type: 'scene_by_env', env: 'server' } },
        ], returnType: 'void'
    },
    {
        method_name: 'sendMessageToServer', arguments: [
            { type: 'TCP_Server', name: 'target' },
            { type: 'any', name: 'message' },
        ], returnType: 'void'
    },
    {
        method_name: 'isNetworkMessage', arguments: [], returnType: 'Boolean'
    },
    {
        method_name: 'listenServerMessage', arguments: [], returnType: 'void'
    },
]);