/*
*
*  CSNetwork
*/

Blockly.Msg["CSNetwork-Server.listenClientMessage"] = "监听客户端问答消息";
Blockly.Msg["CSNetwork-Server.isNetworkMessage"] = "是否网络消息";
Blockly.Msg["CSNetwork-Client.isNetworkMessage"] = "是否网络消息";
Blockly.Msg["CSNetwork-Client.sendMessageToServer"] = "向服务器发送问答消息";
Blockly.Msg["CSNetwork-Server"] = "CS网络服务器端";
Blockly.Msg["CSNetwork-Client"] = "CS网络客户端";
Blockly.Msg["server_sence"] = "服务器场景";
Blockly.Msg["message"] = "消息";

OpenBlock.Env.registerEvents([''], 'csnetwork.h', [
    { "name": "NetworkError", "argType": "String" },
    { "name": "ServiceError", "argType": "String" },
]);
OpenBlock.Env.registerNativeFunction(['server'], 'CSNetwork-Server', 'CSNetwork-Server', [
    {
        method_name: 'listenClientMessage', arguments: [], returnType: 'void'
    },
    {
        method_name: 'isNetworkMessage', arguments: [], returnType: 'Boolean'
    },
]);
OpenBlock.Env.registerNativeFunction([''], 'CSNetwork-Client', 'CSNetwork-Client', [
    {
        method_name: 'sendMessageToServer', arguments: [
            { type: 'String', name: 'server_sence', field: { type: 'scene_by_env', env: 'server' } },
            { type: 'String', name: 'title', field: true },
            { type: 'any', name: 'message' },
            // { type: 'Boolean', name: 'ignoreReply',field: {type:'Boolean'} },
        ], returnType: 'void'
    },
    {
        method_name: 'isNetworkMessage', arguments: [], returnType: 'Boolean'
    },
]);