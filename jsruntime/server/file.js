/*
*
*  file
*/

Blockly.Msg["simplefile.simplefile"] = "简单文件系统";
Blockly.Msg["simplefile.readTextFile"] = "读取文本文件";
Blockly.Msg["simplefile.writeTextFile"] = "写入文本文件";

Blockly.Msg["simplefile.appendTextFile"] = "追加到文本文件";
Blockly.Msg["simplefile.readBinFile"] = "读取二进制文件";

Blockly.Msg["simplefile.writeBinFile"] = "写入二进制文件";
Blockly.Msg["simplefile.appendBinFile"] = "追加到二进制文件";
Blockly.Msg["simplefile.deleteFile"] = "删除文件";
Blockly.Msg["content"] = "内容";
Blockly.Msg["path"] = "路径";

OpenBlock.Env.registerEvents(['server'], 'simplefile', [
    { "name": "文件大小超过限制", "argType": "String" },
]);
OpenBlock.Env.registerNativeFunction(['server'], 'simplefile', 'simplefile', [
    {
        method_name: 'readTextFile', arguments: [
            { type: 'String', name: 'path' },
            { type: 'String', name: 'title', field: true },
        ], returnType: 'void'
    },
    {
        method_name: 'writeTextFile', arguments: [
            { type: 'String', name: 'path' },
            { type: 'String', name: 'content' },
        ], returnType: 'void'
    },
    {
        method_name: 'appendTextFile', arguments: [
            { type: 'String', name: 'path' },
            { type: 'String', name: 'content' },
        ], returnType: 'void'
    },

    {
        method_name: 'readBinFile', arguments: [
            { type: 'String', name: 'path' },
            { type: 'String', name: 'title', field: true },
        ], returnType: 'void'
    },
    {
        method_name: 'writeBinFile', arguments: [
            { type: 'String', name: 'path' },
            { type: 'list<Integer>', name: 'content' },
        ], returnType: 'void'
    },
    {
        method_name: 'appendBinFile', arguments: [
            { type: 'String', name: 'path' },
            { type: 'list<Integer>', name: 'content' },
        ], returnType: 'void'
    },
    {
        method_name: 'deleteFile', arguments: [
            { type: 'String', name: 'path' },
        ], returnType: 'void'
    },
]);