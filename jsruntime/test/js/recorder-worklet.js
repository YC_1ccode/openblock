class RecorderWorklet extends AudioWorkletProcessor {
    constructor() {
        super();
    }

    process(inputs, outputs, parameters) {
        for (let i = 0; i < inputs.length; i++) {
            for (let j = 0; j < inputs[i].length; j++) {
                outputs[i][j].set(inputs[i][j], 0)
            }
        }

        this.port.postMessage(inputs);
        return inputs.length > 0;
    }
}

registerProcessor('recorder-worklet', RecorderWorklet);