# 使用 Node.js 官方基础镜像  
FROM node:alpine3.20

# 设置工作目录  
WORKDIR /app  

# 通过包管理器安装 static-server  
RUN npm install -g static-server  

# 将 Openblock 的文件复制到镜像中  
COPY . /app/openblock  

# 暴露端口9080  
EXPOSE 9080  

# 启动 static-server，指定前端文件所在的目录和端口  
CMD ["static-server", ".", "-p", "9080"]