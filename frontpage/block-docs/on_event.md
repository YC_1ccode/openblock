# 事件

![on_event_start](./img/on_event_start.png)

事件块只显示事件的名称

当事件发生时，执行事件块连接的逻辑。

其他事件描述请参阅集成文档。