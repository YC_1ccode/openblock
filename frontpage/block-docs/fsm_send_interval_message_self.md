# 间隔a毫秒给当前状态机发送消息

参数：
- param1：间隔时间，单位毫秒
- param2：消息名

示例程序：

![alt text](img/fsm_send_message.png)

每间隔100毫秒给当前状态机发送一次消息，接收到消息后输出“hello”。