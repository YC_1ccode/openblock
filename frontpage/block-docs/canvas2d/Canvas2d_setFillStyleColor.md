# 设置填充颜色

参数：
- Number类型的颜色

功能：
- 设置填充颜色

示例：

![alt text](img/setFillStyleColor.png)

绘制了两个实心矩形，一个填充颜色为rgb(100, 50, 0)，一个填充颜色已有颜色绿色。