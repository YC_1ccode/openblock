
const GOTO_BLOCK_SVG_DATAURI =
    'data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgMjQgMjQiIGZpbGw9Im5vbmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgaWQ9IlNWR1JlcG9fYmdDYXJyaWVyIiBzdHJva2Utd2lkdGg9IjAiPjwvZz48ZyBpZD0iU1ZHUmVwb190cmFjZXJDYXJyaWVyIiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiPjwvZz48ZyBpZD0iU1ZHUmVwb19pY29uQ2FycmllciI+IDxwYXRoIGQ9Ik0xNS45ODk0IDQuOTUwMkwxNi41MiA0LjQyMDE0VjQuNDIwMTRMMTUuOTg5NCA0Ljk1MDJaTTE5LjA3MTYgOC4wMzU2MkwxOC41NDEgOC41NjU2OEwxOS4wNzE2IDguMDM1NjJaTTguNzM4MzcgMTkuNDI5TDguMjA3NzcgMTkuOTU5MUw4LjczODM3IDE5LjQyOVpNNC42MjE2OSAxNS4zMDgxTDUuMTUyMjkgMTQuNzc4MUw0LjYyMTY5IDE1LjMwODFaTTE3LjU2NjkgMTQuOTk0M0wxNy4zMDMyIDE0LjI5MjJMMTcuNTY2OSAxNC45OTQzWk0xNS42NDk4IDE1LjcxNDZMMTUuOTEzNiAxNi40MTY3SDE1LjkxMzZMMTUuNjQ5OCAxNS43MTQ2Wk04LjMzMjIgOC4zODE3N0w3LjYyNzk4IDguMTIzNzVMOC4zMzIyIDguMzgxNzdaTTkuMDI2NjUgNi40ODYzNkw5LjczMDg3IDYuNzQ0MzhWNi43NDQzOEw5LjAyNjY1IDYuNDg2MzZaTTUuODQ1MDQgMTAuNjczNUw2LjA0NDM4IDExLjM5NjVMNS44NDUwNCAxMC42NzM1Wk03LjMwMTY3IDEwLjEzNTFMNi44NjM0NiA5LjUyNjQ2TDYuODYzNDYgOS41MjY0Nkw3LjMwMTY3IDEwLjEzNTFaTTcuNjc1ODIgOS43OTAzOEw4LjI0NjY1IDEwLjI3NjhIOC4yNDY2NUw3LjY3NTgyIDkuNzkwMzhaTTE0LjI1MSAxNi4zODA1TDE0Ljc0MiAxNi45NDc1TDE0Ljc0MiAxNi45NDc1TDE0LjI1MSAxNi4zODA1Wk0xMy4zODA2IDE4LjIwMTJMMTIuNjU3NCAxOC4wMDIyVjE4LjAwMjJMMTMuMzgwNiAxOC4yMDEyWk0xMy45MTY5IDE2Ljc0NjZMMTMuMzA3NSAxNi4zMDk0TDEzLjMwNzUgMTYuMzA5NEwxMy45MTY5IDE2Ljc0NjZaTTIuNzE4NDYgMTIuNzU1MkwxLjk2ODQ4IDEyLjc2TDEuOTY4NDggMTIuNzZMMi43MTg0NiAxMi43NTUyWk0yLjkzMDQ1IDExLjk1MjFMMi4yODA1MyAxMS41Nzc4SDIuMjgwNTNMMi45MzA0NSAxMS45NTIxWk0xMS4zMDUyIDIxLjM0MzFMMTEuMzA2NCAyMC41OTMxSDExLjMwNjRMMTEuMzA1MiAyMS4zNDMxWk0xMi4wOTMzIDIxLjEzNDdMMTEuNzIxNSAyMC40ODMzTDExLjcyMTUgMjAuNDgzM0wxMi4wOTMzIDIxLjEzNDdaTTExLjY5NzMgMi4wMzYwNkwxMS44NTg4IDIuNzY4NDVMMTEuNjk3MyAyLjAzNjA2Wk0xLjQ2OTQgMjEuNDY5OUMxLjE3NjY2IDIxLjc2MyAxLjE3NjkgMjIuMjM3OSAxLjQ2OTk0IDIyLjUzMDZDMS43NjI5OCAyMi44MjMzIDIuMjM3ODYgMjIuODIzMSAyLjUzMDYgMjIuNTMwMUwxLjQ2OTQgMjEuNDY5OVpNNy4xODM4MyAxNy44NzIxQzcuNDc2NTcgMTcuNTc5MSA3LjQ3NjMzIDE3LjEwNDIgNy4xODMyOSAxNi44MTE0QzYuODkwMjQgMTYuNTE4NyA2LjQxNTM3IDE2LjUxODkgNi4xMjI2MyAxNi44MTJMNy4xODM4MyAxNy44NzIxWk0xNS40NTg4IDUuNDgwMjZMMTguNTQxIDguNTY1NjhMMTkuNjAyMiA3LjUwNTU2TDE2LjUyIDQuNDIwMTRMMTUuNDU4OCA1LjQ4MDI2Wk05LjI2ODk3IDE4Ljg5ODlMNS4xNTIyOSAxNC43NzgxTDQuMDkxMDkgMTUuODM4Mkw4LjIwNzc3IDE5Ljk1OTFMOS4yNjg5NyAxOC44OTg5Wk0xNy4zMDMyIDE0LjI5MjJMMTUuMzg2IDE1LjAxMjVMMTUuOTEzNiAxNi40MTY3TDE3LjgzMDcgMTUuNjk2NEwxNy4zMDMyIDE0LjI5MjJaTTkuMDM2NDIgOC42Mzk3OUw5LjczMDg3IDYuNzQ0MzhMOC4zMjI0MyA2LjIyODM0TDcuNjI3OTggOC4xMjM3NUw5LjAzNjQyIDguNjM5NzlaTTYuMDQ0MzggMTEuMzk2NUM2Ljc1NTgzIDExLjIwMDMgNy4yOTcxOSAxMS4wNjI1IDcuNzM5ODcgMTAuNzQzOEw2Ljg2MzQ2IDkuNTI2NDZDNi42OTA1MyA5LjY1MDk3IDYuNDY2MDEgOS43MjQyOCA1LjY0NTcgOS45NTA0NEw2LjA0NDM4IDExLjM5NjVaTTcuNjI3OTggOC4xMjM3NUM3LjMzNTAyIDguOTIzMzIgNy4yNDMzOCA5LjE0MTUzIDcuMTA0OTkgOS4zMDM5MUw4LjI0NjY1IDEwLjI3NjhDOC42MDA0MSA5Ljg2MTc1IDguNzgyMyA5LjMzMzM3IDkuMDM2NDIgOC42Mzk3OUw3LjYyNzk4IDguMTIzNzVaTTcuNzM5ODcgMTAuNzQzOEM3LjkyNjk2IDEwLjYwOTEgOC4wOTcxMiAxMC40NTIzIDguMjQ2NjUgMTAuMjc2OEw3LjEwNDk5IDkuMzAzOTFDNy4wMzM3IDkuMzg3NTcgNi45NTI2IDkuNDYyMjkgNi44NjM0NiA5LjUyNjQ2TDcuNzM5ODcgMTAuNzQzOFpNMTUuMzg2IDE1LjAxMjVDMTQuNjk3IDE1LjI3MTQgMTQuMTcxNiAxNS40NTcxIDEzLjc2IDE1LjgxMzVMMTQuNzQyIDE2Ljk0NzVDMTQuOTAyOCAxNi44MDgyIDE1LjExOTIgMTYuNzE1MiAxNS45MTM2IDE2LjQxNjdMMTUuMzg2IDE1LjAxMjVaTTE0LjEwMzcgMTguNDAwMUMxNC4zMjkgMTcuNTgxMyAxNC40MDIxIDE3LjM1NjkgMTQuNTI2MyAxNy4xODM4TDEzLjMwNzUgMTYuMzA5NEMxMi45OTAyIDE2Ljc1MTcgMTIuODUyOSAxNy4yOTE5IDEyLjY1NzQgMTguMDAyMkwxNC4xMDM3IDE4LjQwMDFaTTEzLjc2IDE1LjgxMzVDMTMuNTkwMyAxNS45NjA1IDEzLjQzODQgMTYuMTI2OSAxMy4zMDc1IDE2LjMwOTRMMTQuNTI2MyAxNy4xODM4QzE0LjU4ODcgMTcuMDk2OCAxNC42NjExIDE3LjAxNzUgMTQuNzQyIDE2Ljk0NzVMMTMuNzYgMTUuODEzNVpNNS4xNTIyOSAxNC43NzgxQzQuNTA2MTUgMTQuMTMxMyA0LjA2Nzk5IDEzLjY5MSAzLjc4MzY2IDEzLjMzMzhDMy40OTgzNSAxMi45NzUzIDMuNDY4ODkgMTIuODIwMSAzLjQ2ODQ1IDEyLjc1MDVMMS45Njg0OCAxMi43NkMxLjk3MjE1IDEzLjM0MjIgMi4yNjEyNyAxMy44Mjk3IDIuNjEwMDIgMTQuMjY3OUMyLjk1OTc2IDE0LjcwNzMgMy40NzExNSAxNS4yMTc2IDQuMDkxMDkgMTUuODM4Mkw1LjE1MjI5IDE0Ljc3ODFaTTUuNjQ1NyA5Ljk1MDQ0QzQuODAwNDggMTAuMTgzNSA0LjEwMzk2IDEwLjM3NDMgMy41ODI5NiAxMC41ODM1QzMuMDYzNDEgMTAuNzkyIDIuNTcxMTYgMTEuMDczMiAyLjI4MDUzIDExLjU3NzhMMy41ODAzOCAxMi4zMjY0QzMuNjE1IDEyLjI2NjMgMy43MTY5MyAxMi4xNDYgNC4xNDE4IDExLjk3NTVDNC41NjUyMyAxMS44MDU1IDUuMTYzMzcgMTEuNjM5NCA2LjA0NDM4IDExLjM5NjVMNS42NDU3IDkuOTUwNDRaTTMuNDY4NDUgMTIuNzUwNUMzLjQ2NzUxIDEyLjYwMTYgMy41MDYxNiAxMi40NTUzIDMuNTgwMzggMTIuMzI2NEwyLjI4MDUzIDExLjU3NzhDMi4wNzM1NCAxMS45MzcyIDEuOTY1ODYgMTIuMzQ1MiAxLjk2ODQ4IDEyLjc2TDMuNDY4NDUgMTIuNzUwNVpNOC4yMDc3NyAxOS45NTkxQzguODMxNjQgMjAuNTgzNiA5LjM0NDY0IDIxLjA5ODcgOS43ODY0NyAyMS40NTA2QzEwLjIyNyAyMS44MDE1IDEwLjcxNzkgMjIuMDkyMiAxMS4zMDQxIDIyLjA5MzFMMTEuMzA2NCAyMC41OTMxQzExLjIzNjkgMjAuNTkzIDExLjA4MTQgMjAuNTY0NCAxMC43MjEgMjAuMjc3M0MxMC4zNjE4IDE5Ljk5MTIgOS45MTkyMyAxOS41NDk5IDkuMjY4OTcgMTguODk4OUw4LjIwNzc3IDE5Ljk1OTFaTTEyLjY1NzQgMTguMDAyMkMxMi40MTMzIDE4Ljg4OTcgMTIuMjQ2MiAxOS40OTI0IDEyLjA3NTEgMTkuOTE4OEMxMS45MDMzIDIwLjM0NjcgMTEuNzgyMSAyMC40NDg3IDExLjcyMTUgMjAuNDgzM0wxMi40NjUgMjEuNzg2MUMxMi45NzQgMjEuNDk1NiAxMy4yNTczIDIxLjAwMDQgMTMuNDY3MSAyMC40Nzc1QzEzLjY3NzYgMTkuOTUzMiAxMy44Njk0IDE5LjI1MTYgMTQuMTAzNyAxOC40MDAxTDEyLjY1NzQgMTguMDAyMlpNMTEuMzA0MSAyMi4wOTMxQzExLjcxMTIgMjIuMDkzNyAxMi4xMTE0IDIxLjk4NzkgMTIuNDY1IDIxLjc4NjFMMTEuNzIxNSAyMC40ODMzQzExLjU5NSAyMC41NTU1IDExLjQ1MTkgMjAuNTkzMyAxMS4zMDY0IDIwLjU5MzFMMTEuMzA0MSAyMi4wOTMxWk0xOC41NDEgOC41NjU2OEMxOS42MDQ1IDkuNjMwMjIgMjAuMzQwMyAxMC4zNjk1IDIwLjc5MTcgMTAuOTc4OEMyMS4yMzUzIDExLjU3NzQgMjEuMjg2MyAxMS44OTU5IDIxLjIzMjEgMTIuMTQ2NEwyMi42OTgyIDEyLjQ2MzRDMjIuODg4MSAxMS41ODU0IDIyLjUzODIgMTAuODE2MiAyMS45OTY5IDEwLjA4NTdDMjEuNDYzNSA5LjM2NTkyIDIwLjYzMDUgOC41MzQ4NiAxOS42MDIyIDcuNTA1NTZMMTguNTQxIDguNTY1NjhaTTE3LjgzMDcgMTUuNjk2NEMxOS4xOTIxIDE1LjE4NDkgMjAuMjk0IDE0Ljc3MyAyMS4wNzcxIDE0LjMzODRDMjEuODcxOCAxMy44OTczIDIyLjUwODMgMTMuMzQxNiAyMi42OTgyIDEyLjQ2MzRMMjEuMjMyMSAxMi4xNDY0QzIxLjE3OCAxMi4zOTY4IDIxLjAwMDEgMTIuNjY1NSAyMC4zNDkxIDEzLjAyNjhDMTkuNjg2NSAxMy4zOTQ2IDE4LjcxMTIgMTMuNzYzMiAxNy4zMDMyIDE0LjI5MjJMMTcuODMwNyAxNS42OTY0Wk0xNi41MiA0LjQyMDE0QzE1LjQ4NDEgMy4zODMyIDE0LjY0ODEgMi41NDM1MyAxMy45MjQ2IDIuMDA2MzhDMTMuMTkwOCAxLjQ2MTY1IDEyLjQxNzUgMS4xMDkxMiAxMS41MzU3IDEuMzAzNjdMMTEuODU4OCAyLjc2ODQ1QzEyLjEwODYgMi43MTMzNSAxMi40Mjc3IDIuNzYzMyAxMy4wMzA0IDMuMjEwNzVDMTMuNjQzMyAzLjY2NTc5IDE0LjM4NzYgNC40MDgwMSAxNS40NTg4IDUuNDgwMjZMMTYuNTIgNC40MjAxNFpNOS43MzA4NyA2Ljc0NDM4QzEwLjI1MjUgNS4zMjA3NSAxMC42MTYxIDQuMzM0MDMgMTAuOTgxMiAzLjY2MzE1QzExLjM0MDIgMy4wMDMzOCAxMS42MDkgMi44MjM1NyAxMS44NTg4IDIuNzY4NDVMMTEuNTM1NyAxLjMwMzY3QzEwLjY1NCAxLjQ5ODE5IDEwLjEwMDUgMi4xNDMzMiA5LjY2MzYyIDIuOTQ2MThDOS4yMzI3OCAzLjczNzkzIDguODI2ODggNC44NTE1NCA4LjMyMjQzIDYuMjI4MzRMOS43MzA4NyA2Ljc0NDM4Wk0yLjUzMDYgMjIuNTMwMUw3LjE4MzgzIDE3Ljg3MjFMNi4xMjI2MyAxNi44MTJMMS40Njk0IDIxLjQ2OTlMMi41MzA2IDIyLjUzMDFaIiBmaWxsPSIjMUMyNzRDIj48L3BhdGg+IDwvZz48L3N2Zz4=';
class FlyoutPinComponent {
    /**
     * The unique id for this component.
     * @type {string}
     */
    id = 'FlyoutPinComponent';
    /**
     * Whether this has been initialized.
     * @type {boolean}
     * @private
     */
    initialized_ = false;
    /**
     * Width of the  control.
     * @type {number}
     * @const
     * @private
     */
    WIDTH_ = 32;

    /**
     * Height of the  control.
     * @type {number}
     * @const
     * @private
     */
    HEIGHT_ = 32;

    /**
     * Distance between  control and bottom or top edge of workspace.
     * @type {number}
     * @const
     * @private
     */
    MARGIN_VERTICAL_ = 20;

    /**
     * Distance between  control and right or left edge of workspace.
     * @type {number}
     * @const
     * @private
     */
    MARGIN_HORIZONTAL_ = 20;
    workspace_;
    dom;
    listener;
    autoClose;
    constructor(workspace) {
        this.workspace_ = workspace;
    }
    init() {
        this.autoClose = this.workspace_.getFlyout()?.autoClose;
        this.workspace_.getComponentManager().addComponent({
            component: this,
            weight: 2,
            capabilities: [Blockly.ComponentManager.Capability.POSITIONABLE],
        });
        this.createDom_();
        this.initialized_ = true;
    }
    /**
     * Creates DOM for ui element.
     * @private
     */
    createDom_() {
        this.svgGroup_ = Blockly.utils.dom.createSvgElement(
            Blockly.utils.Svg.IMAGE, {
            'height': this.HEIGHT_ + 'px',
            'width': this.WIDTH_ + 'px',
            'class': 'searchblock_btn',
        });
        this.svgGroup_.setAttributeNS(Blockly.utils.dom.XLINK_NS, 'xlink:href',
            GOTO_BLOCK_SVG_DATAURI);

        Blockly.utils.dom.insertAfter(
            this.svgGroup_, this.workspace_.getBubbleCanvas());

        // Attach listener.
        this.searchblock_btn = Blockly.browserEvents.conditionalBind(
            this.svgGroup_, 'mousedown', null, this.onClick_.bind(this));
    }
    /**
     * Handle click event.
     * @private
     */
    async onClick_() {
        let flyout = this.workspace_.getFlyout();
        if (flyout) {
            flyout.autoClose = this.autoClose = !this.autoClose;
            if (this.autoClose) {
                this.svgGroup_.setAttribute('transform',
                    'translate(' + this.left_ + ',' + this.top_ + ')');
            } else {
                this.svgGroup_.setAttribute('transform',
                    'translate(' + (this.left_ - 10) + ',' + (this.top_ + 10) + ') rotate(-45)');
            }
        }
    }
    /**
     * Positions the element. Called when the window is resized.
     * @param {!MetricsManager.UiMetrics} metrics The workspace metrics.
     * @param {!Array<!Rect>} savedPositions List of rectangles that
     *     are already on the workspace.
    */
    position(metrics, savedPositions) {
        if (!this.initialized_) {
            return;
        }
        const hasVerticalScrollbars = this.workspace_.scrollbar &&
            this.workspace_.scrollbar.canScrollHorizontally();
        const hasHorizontalScrollbars = this.workspace_.scrollbar &&
            this.workspace_.scrollbar.canScrollVertically();

        if (metrics.toolboxMetrics.position === Blockly.TOOLBOX_AT_LEFT ||
            (this.workspace_.horizontalLayout && !this.workspace_.RTL)) {
            // Right corner placement.
            this.left_ = metrics.absoluteMetrics.left + metrics.viewMetrics.width -
                this.WIDTH_ - this.MARGIN_HORIZONTAL_;
            if (hasVerticalScrollbars && !this.workspace_.RTL) {
                this.left_ -= Blockly.Scrollbar.scrollbarThickness;
            }
        } else {
            // Left corner placement.
            this.left_ = this.MARGIN_HORIZONTAL_;
            if (hasVerticalScrollbars && this.workspace_.RTL) {
                this.left_ += Blockly.Scrollbar.scrollbarThickness;
            }
        }

        const startAtBottom =
            metrics.toolboxMetrics.position !== Blockly.TOOLBOX_AT_BOTTOM;
        if (startAtBottom) {
            // Bottom corner placement
            this.top_ = metrics.absoluteMetrics.top + metrics.viewMetrics.height -
                this.HEIGHT_ - this.MARGIN_VERTICAL_;
            if (hasHorizontalScrollbars) {
                // The horizontal scrollbars are always positioned on the bottom.
                this.top_ -= Blockly.Scrollbar.scrollbarThickness;
            }
        } else {
            // Upper corner placement
            this.top_ = metrics.absoluteMetrics.top + this.MARGIN_VERTICAL_;
        }

        // Check for collision and bump if needed.
        let boundingRect = this.getBoundingRectangle();
        for (let i = 0, otherEl; (otherEl = savedPositions[i]); i++) {
            if (boundingRect.intersects(otherEl)) {
                if (startAtBottom) { // Bump up.
                    this.top_ = otherEl.top - this.HEIGHT_ - this.MARGIN_VERTICAL_;
                } else { // Bump down.
                    this.top_ = otherEl.bottom + this.MARGIN_VERTICAL_;
                }
                // Recheck other savedPositions
                boundingRect = this.getBoundingRectangle();
                i = -1;
            }
        }
        if (this.autoClose) {
            this.svgGroup_.setAttribute('transform',
                'translate(' + this.left_ + ',' + this.top_ + ')');
        } else {
            this.svgGroup_.setAttribute('transform',
                'translate(' + (this.left_ - 15) + ',' + this.top_ + ') rotate(-45)');
        }
    }
    /**
     * Returns the bounding rectangle of the UI element in pixel units relative to
     * the Blockly injection div.
     * @return {?Rect} The UI elements's bounding box. Null if
     *   bounding box should be ignored by other UI elements.
     */
    getBoundingRectangle() {
        return new Blockly.utils.Rect(
            this.top_, this.top_ + this.HEIGHT_,
            this.left_, this.left_ + this.WIDTH_);
    }
}
OpenBlock.onInited(() => {
    OpenBlock.wsBuildCbs.push(function (workspace) {
        new FlyoutPinComponent(workspace).init();
    });
    Blockly.Css.register(
        `.flyoutpin_btn {
          opacity: .2;
          display:block;
        }
        .flyoutpin_btn:hover {
          opacity: .4;
        }
        .flyoutpin_btn:active {
          opacity: .6;
        }`
    );
});
